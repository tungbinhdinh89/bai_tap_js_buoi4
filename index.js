// ex_01
function showNumberAscending() {
  var num1 = document.getElementById('num1').value;
  var num2 = document.getElementById('num2').value;
  var num3 = document.getElementById('num3').value;

  if (num1 > num2 && num2 > num3) {
    document.getElementById('result').innerText = `${num3}, ${num2}, ${num1}`;
  } else if (num2 > num1 && num1 > num3) {
    document.getElementById('result').innerText = `${num3}, ${num1}, ${num2}`;
  } else if (num3 > num1 && num1 > num2) {
    document.getElementById('result').innerText = `${num2}, ${num1}, ${num3}`;
  } else if (num1 > num3 && num3 > num2) {
    document.getElementById('result').innerText = `${num2}, ${num3}, ${num1}`;
  } else if (num3 > num2 && num2 > num1) {
    document.getElementById('result').innerText = `${num1}, ${num2}, ${num3}`;
  } else if (num2 > num3 && num3 > num1) {
    document.getElementById('result').innerText = `${num1}, ${num3}, ${num2}`;
  }
}
// ex_02
function greeting() {
  var isWho = document.getElementById('who-is').value;
  console.log(isWho);
  document.getElementById('result2').innerText = `Xin chào: ${isWho} !`;
}
// ex_03
function showNumber() {
  var num_1 = document.getElementById('num-1').value;
  var num_2 = document.getElementById('num-2').value;
  var num_3 = document.getElementById('num-3').value;
  console.log('yes');

  if (num_1 % 2 === 0 && num_2 % 2 === 0 && num_3 % 2 === 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 3 số chẵn là: ${num_1}, ${num_2}, ${num_3}`;
  } else if (num_1 % 2 !== 0 && num_2 % 2 !== 0 && num_3 % 2 !== 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 3 số lẽ là: ${num_1}, ${num_2}, ${num_3}`;
  } else if (num_1 % 2 === 0 && num_2 % 2 === 0 && num_3 % 2 !== 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 2 số chẵn là ${num_1}, ${num_2} và 1 số lẽ là: ${num_3}`;
  } else if (num_1 % 2 === 0 && num_2 % 2 !== 0 && num_3 % 2 === 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 2 số chẵn là: ${num_1}, ${num_3} và 1 số lẽ là: ${num_2}`;
  } else if (num_1 % 2 !== 0 && num_2 % 2 === 0 && num_3 % 2 === 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 2 số chẵn là: ${num_2}, ${num_3} và 1 số lẽ là: ${num_1}`;
  } else if (num_1 % 2 !== 0 && num_2 % 2 === 0 && num_3 % 2 !== 0) {
    document.getElementById(
      'result3'
    ).innerText = `Có 2 số lẽ là: ${num_1}, ${num_3} và 1 số chẵn là: ${num_2}`;
  } else if (num_1 % 2 !== 0 && num_2 % 2 === 0 && num_3 % 2 !== 0) {
    document.getElementnyId(
      'result3'
    ).innerText = `Có 2 số lẽ là: ${num_1}, ${num_2} và 1 số chẵn là: ${num_3}`;
  } else if (num_1 % 2 !== 0 && num_2 % 2 === 0 && num_3 % 2 !== 0) {
    document.getElementnyId(
      'result3'
    ).innerText = `Có 2 số lẽ là: ${num_2}, ${num_3} và 1 số chẵn là: ${num_1}`;
  }
}
// ex_04
function showTriangle() {
  var edge1 = document.getElementById('edge1').value;
  var edge2 = document.getElementById('edge2').value;
  var edge3 = document.getElementById('edge3').value;
  //   console.log('yes');
  if (edge1 === edge2 && edge2 === edge3) {
    document.getElementById('result4').innerText = `Tam Giác Điều`;
  } else if (edge1 === edge2 && edge2 !== edge3) {
    document.getElementById('result4').innerText = `Tam Giác Cân`;
  } else if (edge1 * edge1 + edge2 * edge2 === edge3 * edge3) {
    document.getElementById('result4').innerText = `Tam Giác Vuông`;
  } else {
    document.getElementById('result4').innerText = `Tam Giác Thường`;
  }
}
